#!/bin/bash
#cria o super_usuário do postgres com o nome do usuário logado
clear
USUARIO=echo whoami
sudo -u postgres /usr/bin/createuser -sPE $USUARIO

#criar banco de dados
createdb flask-student-app #comentar caso queria usar uma versão com nome de banco diferente
#createdb $1  ## descomentar para versão em que se entra com o nome do banco
#exempo de utilização da linha a cima: ./criar_db.sh nome_do_banco

echo 'Banco do app Configurado'
echo 'FIM'
