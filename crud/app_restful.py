#!/usr/bin/env python3

from flask import Flask, request, redirect, url_for, render_template
from user import User
from flask_modus import Modus
from flask_restful import Resource, Api

app = Flask(__name__)
modus = Modus(app)
api = Api(app)

users = [User('Mateus', 'Bentes')]

def find_user(user_id):
    return [user for user in users if user.id == user_id][0]

class Root(Resource):
	def get(self):
		return redirect(url_for('index'))

class Index(Resource):
	def post(self):
		new_user = User(request.form['first_name'], request.form['last_name'])
		users.append(new_user)
		return	redirect(url_for('index'))

	def get(self):
		return render_template('index.html', users=users)

class New(Resource):
	def get(self):
		return render_template('new.html')

class Show(Resource):
	def get(self, id):
		return render_template('show.html', user=found_user)
	def patch(self, id):
		found_user = find_user(id)
		found_user.first_name = request.form['first_name']
		found_user.last_name = request.form['last_name']
		return redirect(url_for('index'))
	def delete(self, id):
		found_user = find_user(id)
		users.remove(found_user)
		return redirect(url_for('index'))

class Edit(Resource):
	def get(self,id):
		found_user = find_user(id)
		return render_template('edit.html', user=found_user)

api.add_resource(Root, '/')
api.add_resource(Index, '/users')
api.add_resource(New, '/users/new')
api.add_resource(Show, '/users/<int:id>')
api.add_resource(Edit, '/users/<int:id>/edit')

if __name__ == '__main__':
	app.run(host= '0.0.0.0', debug=True, port=80)
