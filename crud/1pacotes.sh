#!/bin/bash

sudo apt update -qq && apt upgrade -y -qq

sudo apt install libpq-dev python3-dev python3-pip postgresql-server-dev-9.6 postgresql-9.6 -y -qq

sudo pip3 install psycopg2 psycopg2-binary Flask-SQLAlchemy Flask Flask-modus ipython setuptools
