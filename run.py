#!/usr/bin/env python3
from flask import Flask
from crud import app

app.run(host='0.0.0.0', port=8000, debug=True)
