FROM python:3.7.4-buster

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY ./crud/3popular_db.py ./

CMD ["python", "./3popular_db.py"]

COPY . .

CMD ["python", "./run.py"]

EXPOSE 8000